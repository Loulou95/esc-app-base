<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsagesToShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('shops', function($table) {
           $table->integer('usage')->default(0);
           $table->integer('tokens')->default(0);
           
           $table->timestamp('next_token_refresh')->nullable();
           $table->timestamp('next_usage_refresh')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('shops', function($table) {
            $table->dropColumn(['usage', 'tokens', 'next_token_refresh', 'next_usage_refresh']);
        });
    }
}
